﻿using UnityEngine;

namespace Demo2D.TvChef
{
    public class RocketBehaviour : MonoBehaviour
    {
        public float Force = 10;
        public Rigidbody2D Rb;

        void Update()
        {
            var worldSpaceForce = transform.right * Force;
            Rb.AddForce(worldSpaceForce);
        }
    }
}
