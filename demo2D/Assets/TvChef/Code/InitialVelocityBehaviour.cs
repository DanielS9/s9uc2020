﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demo2D.TvChef
{
    public class InitialVelocityBehaviour : MonoBehaviour
    {
        public float InitialVelocity = 15;
        public Rigidbody2D Rb;

        // Start is called before the first frame update
        void Start()
        {
            Debug.Log(name + " fired with velocity " + InitialVelocity);
            Rb.velocity = InitialVelocity * transform.up;
        }
    }
}
