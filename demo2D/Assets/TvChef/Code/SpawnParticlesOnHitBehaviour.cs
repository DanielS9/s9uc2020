﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnParticlesOnHitBehaviour : MonoBehaviour
{
    public ParticleSystem ParticleSystem;
    public float MinVelocityToPlay = 5;
    private bool _hasPlayed;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.magnitude > MinVelocityToPlay)
            ParticleSystem.Play();
    }
}
