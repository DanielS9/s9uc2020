﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

namespace Demo2D.TvChef
{
    public class CannonBehaviour : MonoBehaviour
    {
        public GameObject CannonBallPrefab;
        public Transform Muzzle;
        public float MaxMuzzleVelocity = 20;
        public float VelocityBuildup = 5;
        private float _currentMuzzleVelocity;
        
        void Update()
        {
            Aim();
            if (Input.GetKey(KeyCode.Space))
            {
                _currentMuzzleVelocity += Time.deltaTime * VelocityBuildup;
                Debug.Log("Current muzzle velocity: " + _currentMuzzleVelocity);
            }

            if (Input.GetKeyUp(KeyCode.Space))
                Fire();
        }

        private void Aim()
        {
            var pointerPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pointerPosition.z = transform.position.z;
            var directionToPointer = (pointerPosition - transform.position).normalized;
            transform.up = directionToPointer;
        }

        public void Fire()
        {
            Debug.Log(name + " fire");
            var cannonBall = Instantiate(CannonBallPrefab);
            cannonBall.transform.position = Muzzle.position;
            cannonBall.transform.rotation = Muzzle.rotation;

            var velocityBehaviour = cannonBall.GetComponent<InitialVelocityBehaviour>();
            velocityBehaviour.InitialVelocity = _currentMuzzleVelocity;
            _currentMuzzleVelocity = 0;
        }
    }
}