# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Steps to create a new project.

* Download project and open it.
* Unity will import the project and download any packages it is dependent on.
	- Universal render pipeline.
	- Input System.
	- AR Foundation. * using 4.0.2.
	- ARKit XR Plugin.
	- ARCore XR Plugin.
		- and their dependencies...
* Create a new univseral pipeline asset.
* Select the created Renderer asset and add the AR Background renderer feature.
* Go to Edit->Project settings->Graphics
* Drag the newly created pipeline asset to the Scriptable Render Pipeline Settings.
* Now go to the XR plug-in management tab and in the respective platform tabs enable the ARCore and ARKit plugins.
* After that go to the XR tab and create settings for ARKit and ARCore. Both should have their requirement set to Required.
* (Optional) Go to the player tab and for mobile platforms turn off auto-rotation.
* Setup a new scene.
* Delete the main camera.
* From the hierarchy context menu add the following
	- AR Session Origin and AR Session objects.
	- Event System
* Add plane detection manager component to the AR Session origin object.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact