﻿using UnityEngine;
using UnityEngine.InputSystem;
using Dice;

namespace DiceInput
{
    /// <summary>
    /// Handles input for the dice scene.
    /// </summary>
    public class PlayerInputHandler : MonoBehaviour
    {
        [SerializeField] private DiceSpawner diceSpawner = null;
        [SerializeField, Range(0, 1)] private float percentageClickableFromOrigin = 0.9f;
        private PlayerInput playerInput = null;
        private InputAction spawnAction = null;
        private InputAction positionAction = null;
        private Vector2 pointerScreenPosition;
        private Vector2 middleScreenPosition;
        private float maxDistance;
        private Camera mainCamera;

        private void Awake()
        {
            mainCamera = Camera.main;
            playerInput = GetComponent<PlayerInput>();
            maxDistance = Mathf.Min(Screen.width, Screen.height) * percentageClickableFromOrigin * 0.5f;
            middleScreenPosition = new Vector2(Screen.width, Screen.height) / 2f;
            spawnAction = playerInput.actions["Player/SpawnDice"];
            positionAction = playerInput.actions["Player/PointerPosition"];
        }

        private void OnEnable()
        {
            spawnAction.performed += OnClicked;
            positionAction.performed += OnPositionUpdated;
        }

        private void OnDisable()
        {
            spawnAction.performed -= OnClicked;
            spawnAction.performed -= OnPositionUpdated;
        }

        private void OnClicked(InputAction.CallbackContext callbackContext)
        {
            if (PointerCloseToCenter() && callbackContext.performed)
            {
                var cameraRayDirection = mainCamera.ScreenPointToRay(pointerScreenPosition).direction;
                diceSpawner.SpawnDice(mainCamera.transform.position, cameraRayDirection);
            }

            bool PointerCloseToCenter()
            {
                return Vector2.Distance(pointerScreenPosition, middleScreenPosition) < maxDistance;
            }
        }

        private void OnPositionUpdated(InputAction.CallbackContext callbackContext)
        {
            pointerScreenPosition = callbackContext.ReadValue<Vector2>();
        }
    }
}