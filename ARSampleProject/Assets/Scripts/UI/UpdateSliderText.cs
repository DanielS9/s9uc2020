﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DiceUI
{
    /// <summary>
    /// Updates a text showing the slider value.
    /// </summary>
    public class UpdateSliderText : MonoBehaviour
    {
        [SerializeField, Tooltip("The number we want to update.")]
        private TextMeshProUGUI sliderNumberText = null;
        
        [SerializeField, Tooltip("Reference to the slider.")]
        private Slider slider = null;

        private void Awake()
        {
            sliderNumberText.text = slider.value.ToString();
            slider.onValueChanged.AddListener(SliderUpdate);
        }

        private void SliderUpdate(float value)
        {
            sliderNumberText.text = value.ToString();
        }
    }
}