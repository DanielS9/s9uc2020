﻿using UnityEngine;

namespace Dice
{
    /// <summary>
    /// Handles logic for a dice.
    /// </summary>
    public class Dice : MonoBehaviour
    {
        [SerializeField, Tooltip("Time until a die is destroyed.")]
        private float timeToDestroy = 0f;

        [SerializeField, Range(0, 10), Tooltip("How far can a dice be from the camera before being destroyed.")]
        private float maximumDistanceFromCamera = 1f;

        [SerializeField, Tooltip("How often we check if the dice is too far away from the camera.")]
        private float timeToCheckCameraDistance = 0.5f;

        private bool cleaningUp = false;
        private Camera mainCamera = null;

        private void Awake()
        {
            mainCamera = Camera.main;
        }

        private void Start()
        {
            InvokeRepeating("DestroyIfTooFarFromCamera", 0, timeToCheckCameraDistance);
        }

        private void DestroyIfTooFarFromCamera()
        {
            if (Vector3.Distance(mainCamera.transform.position, transform.position) > maximumDistanceFromCamera && !cleaningUp)
            {
                StartCleanup();
            }
        }

        /// <summary>
        /// Starts the process of cleaning up a dice.
        /// </summary>
        public void StartCleanup()
        {
            GetComponent<DiceFader>().StartFade(timeToDestroy);
            if (!cleaningUp)
            {
                Destroy(gameObject, timeToDestroy);
                cleaningUp = true;
            }
        }
    }
}