﻿using UnityEngine;

namespace Dice
{
    /// <summary>
    /// Spawns dice, given a range of parameters.
    /// </summary>
    public class DiceSpawner : MonoBehaviour
    {
        [SerializeField, Tooltip("The prefab to spawn.")]
        private GameObject dicePrefab = null;

        [SerializeField, Range(0, 1000), Tooltip("The maximum amount of force to apply.")]
        private float maximumForce;

        [SerializeField, Range(0, 1), Tooltip("How much we randomly reduce the force in order to create variance.")]
        private float maximumForceReduction;

        [SerializeField, Range(0, 10000), Tooltip("How much angular torque we add.")]
        private float maximumTorque;

        [SerializeField, Tooltip("How much we offset the spawn position, in order to not spawn the dice inside the camera.")]
        private float spawnOffset;

        [SerializeField, Range(0, 1), Tooltip("How much we randomly offset the dice direction.")]
        private float directionRandomness;

        private int numberOfDiceToThrow = 1;
        private Vector3 randomDirection;

        /// <summary>
        /// Spawns a dice.
        /// </summary>
        /// <param name="origin">The dice origin.</param>
        /// <param name="direction">The direction we're throwing it in.</param>
        public void SpawnDice(Vector3 origin, Vector3 direction)
        {
            foreach (Transform child in transform)
            {
                child.GetComponent<Dice>().StartCleanup();
            }

            for (int i = 0; i < numberOfDiceToThrow; i++)
            {
                CreateNewDice();
            }

            void CreateNewDice()
            {
                randomDirection = (Random.onUnitSphere * directionRandomness + direction).normalized;
                var diceInstance = Instantiate(dicePrefab, origin + randomDirection * spawnOffset, Random.rotation);
                diceInstance.transform.parent = transform;
                var forceApplied = maximumForce - Random.Range(0, maximumForceReduction) * maximumForce;
                var body = diceInstance.GetComponent<Rigidbody>();
                body.AddForce(randomDirection * forceApplied);
                body.AddTorque(Random.onUnitSphere * maximumTorque, ForceMode.Impulse);
            }
        }

        /// <summary>
        /// We call this method when the slider value updates.
        /// </summary>
        /// <param name="number">The amount of dice to spawn.</param>
        public void OnSliderUpdate(float number)
        {
            numberOfDiceToThrow = (int)number;
        }
    }
}