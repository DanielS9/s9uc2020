﻿using System.Collections;
using UnityEngine;

namespace Dice
{
    public class DiceFader : MonoBehaviour
    {
        [SerializeField, Tooltip("A reference to the dice renderer.")]
        private Renderer diceRenderer;
        private Material diceMaterial;
        private float timeToFadeout;
        private float time;
        private float currentDissolve;
        private const float minDissolve = -1;
        private const float maxDissolve = 1;

        private void Awake()
        {
            diceMaterial = diceRenderer.material;
        }

        public void StartFade(float timeToFadeout)
        {
            this.timeToFadeout = timeToFadeout;
            StartCoroutine(Fade());
        }

        private IEnumerator Fade()
        {
            while (true)
            {
                currentDissolve = Mathf.Lerp(minDissolve, maxDissolve, time / timeToFadeout);
                diceMaterial.SetFloat("_Dissolve", currentDissolve);
                time += Time.deltaTime;
                yield return null;
            }
        }
    }
}